import CanvasInitializer from './canvas-initalizer.js'

export class CanvasShapeDrawer{
    constructor(){
     this.shapes = [];   
     this.context = CanvasInitializer.context;
    }
    //add shape to shapes colletions
    addShape(shape){
        this.shapes.push(shape);
    }

    //draw shapes from the shape collection
    drawShapes(){
        CanvasInitializer.clearCanvas()
        for(var i = 0; i < this.shapes.length; i++){
            this.shapes[i].draw(this);
        }        
    }

    //Inititate the constant animation (after this method is called the canvas will redraw constantly)
    animateShapes(){               
        window.requestAnimationFrame(this.drawShapes.bind(this));
        this.animateShapes();
    }
    
    //draws a simple rectangle on a canvas
    drawRectangle(positionX, positionY, sizeX, sizeY, colorHex){
        this.context = CanvasInitializer.context;
        if(colorHex){
            this.context.fillStyle = colorHex;
        }
        this.context.fillRect(positionX, positionY, sizeX, sizeY);
        this.context.fillStyle = "black";

    }
}