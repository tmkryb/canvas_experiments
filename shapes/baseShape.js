//base class - every shape should inherit from it, and implement the draw function
export default class BaseShape{
    draw(shapeDrawer){
        throw new Error('You have to implement the method draw and pass parameter shapeDrawer!');s        
    }
}