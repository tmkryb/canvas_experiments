import { CanvasShapeDrawer } from '../canvas-shape-drawer.js'
import BaseShape from './baseShape.js';
//Small orange rectangle class
export class OrangeRect extends BaseShape{
    constructor(){
        super();
        this.positionX = 25;
        this.positionY = 25;
        this.sizeX = 25;
        this.sizeY = 25;        
    }

    //Implementation of draw function from BaseShape
    draw(shapeDrawer){
        shapeDrawer.drawRectangle(this.positionX, this.positionY, this.sizeX, this.sizeY, "orange");
    }
}