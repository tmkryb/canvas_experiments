import BaseShape from "./baseShape.js";

//red big rectangle shape
export class RedBigRect extends BaseShape{
    constructor() {
        super();
        this.positionX = 100;
        this.positionY = 100;
        this.sizeX = 100;
        this.sizeY = 100;
    }

    //Implementation of draw function from BaseShape
    draw(shapeDrawer){
        shapeDrawer.drawRectangle(this.positionX, this.positionY, this.sizeX, this.sizeY, "red");
    }
}