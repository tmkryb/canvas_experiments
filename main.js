import CanvasInitializer from './canvas-initalizer.js'
import { CanvasShapeDrawer } from './canvas-shape-drawer.js'
import { OrangeRect } from './shapes/orangeRect.js';
import { RedBigRect } from './shapes/redBigRect.js';

export class Main{
    constructor(){
        //Create the shape drawer
        this.shapeDrawer = new CanvasShapeDrawer();
        //initalize canvas for html canvas tag with id "mainCanvas"
        CanvasInitializer.init("mainCanvas");
        //Create the orange rect
        this.orangeRect = new OrangeRect();        
        //Create the red rect
        this.redRect = new RedBigRect();

        //add the orange rect to shapes collection
        this.shapeDrawer.addShape(this.orangeRect);        
        
        //listen for keydown event on window
        window.onkeydown = (e) => {

            //this changes the orangeRect position, and requests a canvas redraw with new data
            //this redraw only when the key is pressed
            if(e.key == "ArrowLeft"){
                this.orangeRect.positionX -= 5;
            }
            if(e.key == "ArrowRight"){
                this.orangeRect.positionX += 5;
            }
            if(e.key == "ArrowUp"){
                this.orangeRect.positionY -= 5;
            }
            if(e.key == "ArrowDown"){
                this.orangeRect.positionY += 5;
            }
            window.requestAnimationFrame(this.shapeDrawer.drawShapes.bind(this.shapeDrawer))
            console.log(e);
        };

        //in this implementation I'm not using the animateShapes function from shapeDrawer - to move a block on keypress is much more efficient to update it
        //only when the key is pressed. You can use animateShapes, and for example make and interval that will change the position every second, then it will 
        //animate automatically, but will consume much resources because it will redraw constantly
        
    }    
}

var main = new Main();
