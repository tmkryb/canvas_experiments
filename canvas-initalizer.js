let instance = null;

class CanvasInitializer {    
    constructor(){
        //Singleton style - return always the same object instance

        if(!instance){
            instance = this;
        }
        return instance;
    }
    //Initializes canvas with size, gets the context
    init(canvasId){
        var canvas = document.getElementById(canvasId);
        canvas.width = 640;
        canvas.height = 480;
        this.context = canvas.getContext('2d');
    }
    //clear the canvas
    clearCanvas(){
        this.context.clearRect(0, 0, 640, 480);
    }
}

export default new CanvasInitializer();